package kz.aitu.demo.controller;

import kz.aitu.demo.repository.GroupRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupController {
    private final GroupRepository groupRepository;

    public GroupController(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }
    @GetMapping("/group/order/{id}")
    public ResponseEntity<?> getGroupById(@PathVariable long id) {
        return ResponseEntity.ok(groupRepository.findById(id));
    }
    @GetMapping("/group")
    public ResponseEntity<?> getGroups() {
        return ResponseEntity.ok(groupRepository.findAll());
    }
}