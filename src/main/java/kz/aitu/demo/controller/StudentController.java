package kz.aitu.demo.controller;

import kz.aitu.demo.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
    private final StudentRepository studentRepository;
    public StudentController(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
    @GetMapping("/student/order/{groupId}")
    public ResponseEntity<?> getStudentsById(@PathVariable long groupId){
        return ResponseEntity.ok(studentRepository.findAllByGroupId(groupId));
    }
    @GetMapping("/student")
    public ResponseEntity<?> getStudents(){
        return ResponseEntity.ok(studentRepository.orderAllByGroupId());
    }

}
