package kz.aitu.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "student_tb")
public class Student {
    @Id
    private long student_id;
    @Column
    private String name;
    private String phone;
    private long groupId;

}
